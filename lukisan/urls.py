from django.conf.urls import url
from django.urls import include, path
from . import views

app_name = "lukisan"

urlpatterns = [
    path('', views.lukisan, name='lukisan'),
]