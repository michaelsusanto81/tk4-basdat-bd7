from django.shortcuts import render, redirect
from django.db import connection

# Create your views here.
def lukisan(request):
	if 'no_staff' not in request.session:
		return redirect('/login/')
	query = """
	SELECT id_lukisan, nama_lukisan, tema_lukisan,
		harga, tahun, id_pelanggan
	FROM LUKISAN;
	"""
	cursor = connection.cursor()
	cursor.execute("SET search_path TO LUKISAN;")
	cursor.execute(query)

	data_lukisan = fetch(cursor)
	return render(request, 'lukisan.html', {'data_lukisan':data_lukisan})

def fetch(cursor):
	columns = [col[0] for col in cursor.description]
	return [dict(zip(columns, row)) for row in cursor.fetchall()]