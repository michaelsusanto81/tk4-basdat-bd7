from django.shortcuts import render, redirect

def home(request):
	if 'no_staff' not in request.session:
		return redirect('/login/')
	response = {}
	response['no_staff'] = request.session['no_staff']
	return render(request, "homepage.html", response)