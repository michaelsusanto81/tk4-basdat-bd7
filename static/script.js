// $(function() {
// 	$('.menu-pengguna').hover(function() {
		// $('.menu-pengguna').append('<h3><a href="#">Penyewa</a></h3>');
		// $('.menu-pengguna').append('<h3><a href="#">Pemilik</a></h3>');
		// $('.menu-pengguna').append('<h3><a href="#">Staff</a></h3>');
// 	});
// 	$('.menu-transaksi').hover(function() {
// 		$('.menu-transaksi').append('<h3><a href="#">Sewa</a></h3>');
// 		$('.menu-transaksi').append('<h3><a href="#">Fee</a></h3>');
// 		$('.menu-transaksi').append('<h3><a href="#">Mitra</a></h3>');
// 	});
// })

$(function() {
	var menuPenggunaIsHidden = true;
	var menuTransaksiIsHidden = true;

	$('#menu-pengguna').click(function() {
		if (menuPenggunaIsHidden) {
			menuPenggunaIsHidden = false;
			// $('#menu-pengguna-dropdown').css('display':'flex');
			$('#menu-pengguna-dropdown').show();
		} else {
			menuPenggunaIsHidden = true;
			// $('#menu-pengguna-dropdown').css('display':'none');
			$('#menu-pengguna-dropdown').hide();
		}
	});
	$('#menu-transaksi').click(function() {
		if (menuTransaksiIsHidden) {
			menuTransaksiIsHidden = false;
			// $('#menu-transaksi-dropdown').css('display':'flex');
			$('#menu-transaksi-dropdown').show();
		} else {
			menuTransaksiIsHidden = true;
			// $('#menu-transaksi-dropdown').css('display':'none');
			$('#menu-transaksi-dropdown').hide();
		}
	})
})