from django.shortcuts import render, redirect

# Create your views here.
def pelanggan(request):
	if 'no_staff' not in request.session:
		return redirect('/login/')
	return render(request, 'pelanggan.html')