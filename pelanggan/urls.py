from django.conf.urls import url
from django.urls import include, path
from . import views

app_name = "pelanggan"

urlpatterns = [
    path('', views.pelanggan, name='pelanggan'),
]