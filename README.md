# Tugas Kelompok 4 - Basis Data - BD7 - LUKISAN

####  1. Mauludi Afina Mirza - 1606884836 
####  2. Michael Susanto - 1806205653 
####  3. Mochamad Naufal Dzulfikar - 1706043903 
####  4. Muhamad Nicky Salim - 1706039572  
####  5. Muhammad Afiful Amin – 1706039963 

# Instalasi dan Konfigurasi Repo ini
- Clone repository ini
```
git clone https://gitlab.com/michaelsusanto81/tk4-basdat-bd7
```

- Buat virtual environment
```
python -m venv env
```

- Jalankan virtual environment
```
./env/Scrips/activate
```

- Masuk ke repository yang telah diclone lewat terminal/cmd, jalankan
```
pip install -r requirements.txt
```

- Silahkan menyesuaikan konfigurasi database di settings.py
```
Pada bagian DATABASES, ubah konfigurasi:
'OPTIONS' : sesuaikan search_path dengan schema yang ada di database masing-masing.
'ENGINE' : isi dengan jenis database yang digunakan
'NAME' : nama database yang digunakan
'USER' : nama user yang login ke database tersebut
'PASSWORD' : password user yang login ke database tersebut
'HOST' : host yang menggunakan database tersebut
'PORT' : (default port pada postgresql adalah 5432)
```

- Database yang digunakan adalah database dari Tugas Kelompok 3 (mengenai LUKISAN)

- Export database (jika diperlukan), dilakukan dengan cara:
```
pg_dump -U <username> <database> > <file>.pgsql
```

- Export database (untuk remote server):
```
pg_dump -U <username> <database> -h <host> > <file>.pgsql
```

- Import database dilakukan dengan cara:
```
psql -U <username> <database> < <file>.pgsql
```

- Jangan lupa menjalankan migrate
```
python manage.py migrate
```

- Jalankan Django server
```
python manage.py runserver
```

# Cara menggunakan Database Web Application ini
- Gunakan salah satu informasi staff di database untuk login
```
(contoh salah satu staff dari database)
no_staff : 190-1-0001
email : gbosher0@unblog.fr
```

# Framework Yang Digunakan

#### 1. Django 2.2.7
#### 2. Bootstrap 4
#### 3. JQuery 3.3.1