from django.conf.urls import url
from django.urls import include, path
from . import views

app_name = "penyewa"

urlpatterns = [
    path('', views.penyewa, name='penyewa'),
]