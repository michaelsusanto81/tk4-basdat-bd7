from django.shortcuts import render, redirect
from django.db import connection

# Create your views here.
def penyewa(request):
	if 'no_staff' not in request.session:
		return redirect('/login/')
	query = """
	SELECT P.id_pelanggan, P.no_ktp, P.nama, P.alamat,
		P.ttl, P.no_hp, P.email, PY.pekerjaan, PY.kategori
	FROM PELANGGAN P JOIN PENYEWA PY ON
		P.id_pelanggan = PY.id_pelanggan;
	"""
	cursor = connection.cursor()
	cursor.execute("SET search_path TO LUKISAN;")
	cursor.execute(query)

	data_penyewa = fetch(cursor)
	return render(request, 'penyewa.html', {'data_penyewa':data_penyewa})

# converts sql rows to a dict
def fetch(cursor):
	columns = [col[0] for col in cursor.description]
	return [dict(zip(columns, row)) for row in cursor.fetchall()]