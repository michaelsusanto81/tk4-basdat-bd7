from django.conf.urls import url
from django.urls import include, path
from . import views

app_name = "mitra"

urlpatterns = [
    path('', views.mitra, name='mitra'),
]