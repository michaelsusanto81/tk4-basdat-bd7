from django.shortcuts import render, redirect
from django.db import connection

# Create your views here.
def mitra(request):
	if 'no_staff' not in request.session:
		return redirect('/login/')
	query = """SELECT * FROM MITRA_BANK;"""
	cursor = connection.cursor()
	cursor.execute("SET SEARCH_PATH TO LUKISAN;")
	cursor.execute(query)

	data_mitra = fetch(cursor)
	return render(request, 'mitra.html', {'data_mitra':data_mitra})

def fetch(cursor):
	columns = [col[0] for col in cursor.description]
	return [dict(zip(columns, row)) for row in cursor.fetchall()]