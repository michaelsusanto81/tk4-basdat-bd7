from django.conf.urls import url
from django.urls import include, path
from . import views

app_name = "transaksi"

urlpatterns = [
    path('', views.transaksi, name='transaksi'),
]