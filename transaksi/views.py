from django.shortcuts import render, redirect
from django.db import connection

# Create your views here.
def transaksi(request):
	if 'no_staff' not in request.session:
		return redirect('/login/')
	query = """
	SELECT TS.tgl_sewa, TS.id_pelanggan, TS.banyaknya, TS.id_lukisan,
		TB.tenor_pilihan, TS.lama_sewa, BS.alamat_tujuan, BS.ongkir,
		BS.total_biaya, TS.tgl_kembali, MB.nama_bank, TS.no_staff
	FROM ((TRANSAKSI_SEWA TS JOIN BIAYA_SEWA BS ON
			TS.tgl_sewa = BS.tgl_sewa AND TS.id_pelanggan = BS.id_pelanggan)
		JOIN TENOR_BAYAR TB ON
			BS.kode_tenor = TB.kode_tenor)
		JOIN MITRA_BANK MB ON
			TB.kode_bank = MB.kode_bank;
	"""
	cursor = connection.cursor()
	cursor.execute("SET SEARCH_PATH TO LUKISAN;")
	cursor.execute(query)

	data_transaksi = fetch(cursor)
	return render(request, 'transaksi.html', {'data_transaksi':data_transaksi})

def fetch(cursor):
	columns = [col[0] for col in cursor.description]
	return [dict(zip(columns, row)) for row in cursor.fetchall()]