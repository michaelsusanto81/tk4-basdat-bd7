from django.conf.urls import url
from django.urls import include, path
from . import views

app_name = "staff"

urlpatterns = [
    path('', views.staff, name='staff'),
]