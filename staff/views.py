from django.shortcuts import render, redirect
from django.db import connection

# Create your views here.
def staff(request):
	if 'no_staff' not in request.session:
		return redirect('/login/')
	query = """
	SELECT S.no_staff, S.nama, S.ttl, S.no_hp,
		S.email, SL.lulusan_staff
	FROM STAFF S JOIN STAFF_LULUSAN SL ON
		S.no_staff = SL.no_staff;
	"""
	cursor = connection.cursor()
	cursor.execute("SET search_path TO LUKISAN;")
	cursor.execute(query)

	data_staff = fetch(cursor)
	return render(request, 'staff.html', {'data_staff':data_staff})

def fetch(cursor):
	columns = [col[0] for col in cursor.description]
	return [dict(zip(columns, row)) for row in cursor.fetchall()]