from django.conf.urls import url
from django.urls import include, path
from . import views

app_name = "fee"

urlpatterns = [
    path('', views.fee, name='fee'),
]