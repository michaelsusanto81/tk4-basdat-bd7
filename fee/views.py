from django.shortcuts import render, redirect
from django.db import connection

# Create your views here.
def fee(request):
	if 'no_staff' not in request.session:
		return redirect('/login/')
	query = """
	SELECT F.tgl_terima, F.id_pelanggan, L.id_lukisan,
		TS.banyaknya, L.harga, F.jumlah
	FROM (FEE F JOIN LUKISAN L ON
			F.id_pelanggan = L.id_pelanggan)
		JOIN TRANSAKSI_SEWA TS ON
			L.id_lukisan = TS.id_lukisan;
	"""
	cursor = connection.cursor()
	cursor.execute("SET SEARCH_PATH TO LUKISAN;")
	cursor.execute(query)

	data_fee = fetch(cursor)
	return render(request, 'fee.html', {'data_fee':data_fee})

def fetch(cursor):
	columns = [col[0] for col in cursor.description]
	return [dict(zip(columns, row)) for row in cursor.fetchall()]