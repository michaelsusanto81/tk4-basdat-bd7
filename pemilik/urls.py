from django.conf.urls import url
from django.urls import include, path
from . import views

app_name = "pemilik"

urlpatterns = [
    path('', views.pemilik, name='pemilik'),
]