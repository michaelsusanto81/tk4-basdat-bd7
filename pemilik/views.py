from django.shortcuts import render, redirect
from django.db import connection

# Create your views here.
def pemilik(request):
	if 'no_staff' not in request.session:
		return redirect('/login/')
	query = """
	SELECT P.id_pelanggan, P.no_ktp, P.nama, P.alamat,
		P.ttl, P.no_hp, P.email, PM.no_rek, PM.nama_bank
	FROM PELANGGAN P JOIN PEMILIK PM ON
		P.id_pelanggan = PM.id_pelanggan;
	"""
	cursor = connection.cursor()
	cursor.execute("SET search_path TO LUKISAN;")
	cursor.execute(query)

	data_pemilik = fetch(cursor)
	return render(request, 'pemilik.html', {'data_pemilik':data_pemilik})

# converts sql rows to a dict
def fetch(cursor):
	columns = [col[0] for col in cursor.description]
	return [dict(zip(columns, row)) for row in cursor.fetchall()]