from django import forms

class LoginForm(forms.Form):
	no_staff = forms.CharField(label='No. Staff', max_length=10)
	email = forms.CharField(label='Email', max_length=255)