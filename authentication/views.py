from django.shortcuts import render, redirect
from django.db import connection
from .forms import LoginForm

# Create your views here.
def login(request):
	if 'no_staff' in request.session:
		return redirect('/')
	response = {}
	response['error'] = False

	form = LoginForm(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['no_staff'] = request.POST['no_staff']
		response['email'] = request.POST['email']

		if(verified(response)):
			print('masuk sini')
			request.session['no_staff'] = response['no_staff']
			request.session['email'] = response['email']
			return redirect('/')
		else:
			response['error'] = True

	response['form'] = form
	return render(request, 'login.html', response)

def logout(request):
	request.session.flush()
	return redirect('/login/')

def verified(data):
	query = """
	SELECT S.no_staff, S.nama, S.ttl, S.no_hp,
		S.email, SL.lulusan_staff
	FROM STAFF S JOIN STAFF_LULUSAN SL ON
		S.no_staff = SL.no_staff;
	"""
	cursor = connection.cursor()
	cursor.execute("SET search_path TO LUKISAN;")
	cursor.execute(query)

	data_staff = fetch(cursor) # list of dictionaries
	for dict_staff in data_staff:
		no_staff_verified = data['no_staff'] == dict_staff['no_staff']
		email_verified = data['email'] == dict_staff['email']
		if(no_staff_verified and email_verified):
			return True
	return False

def fetch(cursor):
	columns = [col[0] for col in cursor.description]
	return [dict(zip(columns, row)) for row in cursor.fetchall()]